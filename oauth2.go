package fundtastic

import (
	"bytes"
	"fmt"
	"github.com/gin-gonic/gin"
	ginserver "github.com/go-oauth2/gin-server"
	"gopkg.in/oauth2.v3/manage"
	"gopkg.in/oauth2.v3/models"
	"gopkg.in/oauth2.v3/server"
	"gopkg.in/oauth2.v3/store"
	"net/http"
)

var AccessTokenRes []interface{}

type bodyLogWriter struct {
	gin.ResponseWriter
	body *bytes.Buffer
}

func (w bodyLogWriter) Write(b []byte) (int, error) {
	w.body.Write(b)
	return w.ResponseWriter.Write(b)
}

func (w bodyLogWriter) WriteString(s string) (int, error) {
	w.body.WriteString(s)
	return w.ResponseWriter.WriteString(s)
}

func ginBodyLogMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		blw := &bodyLogWriter{body: bytes.NewBufferString(""), ResponseWriter: c.Writer}
		c.Writer = blw
		c.Next()

		res := blw.body.String()
		fmt.Println("Response body: " + res)
		//AccessTokenRes = append(AccessTokenRes, res)
		//fmt.Println(AccessTokenRes)
	}
}

func StoreAccessToken() {
	//time.Sleep(time.Duration(rand.Int()%10+1) * time.Second)
	fmt.Println(AccessTokenRes)
}

func OauthManager(clientId string, clientSecret string, domain string) {
	manager := manage.NewDefaultManager()

	// token store
	manager.MustTokenStorage(store.NewFileTokenStore("data.db"))

	// client store
	clientStore := store.NewClientStore()
	err := clientStore.Set("000000", &models.Client{
		ID:     clientId,
		Secret: clientSecret,
		Domain: domain,
	})
	if err != nil {
		return
	}
	manager.MapClientStorage(clientStore)

	// Initialize the oauth2 service
	ginserver.InitServer(manager)
	ginserver.SetAllowGetAccessRequest(true)
	ginserver.SetClientInfoHandler(server.ClientFormHandler)

	g := gin.Default()

	auth := g.Group("/oauth2", ginBodyLogMiddleware())
	{
		auth.GET("/token", ginserver.HandleTokenRequest)
		// localhost:port/token?grant_type=client_credentials&client_id=CLIENT_ID&client_secret=CLIENT_SECRET

	}

	//

	api := g.Group("/api")
	{
		api.Use(ginserver.HandleTokenVerify())
		api.GET("/test", func(c *gin.Context) {
			ti, exists := c.Get(ginserver.DefaultConfig.TokenKey)
			if exists {
				c.JSON(http.StatusOK, ti)
				return
			}
			c.String(http.StatusOK, "not found")
		})
	}

	err = g.Run(":9096")
	if err != nil {
		return
	}
}
