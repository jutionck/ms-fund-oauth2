package fundtastic

import "time"

type Oauth2Config struct {
	GrantType      string
	ClientId       string
	ClientSecret   string
	AccessTokenUri string
	Scope          string
	Username       string
	Password       string
}

type AccessToken struct {
	ExpiresIn    time.Duration
	TokenType    string
	Token        string
	RefreshToken string
}
